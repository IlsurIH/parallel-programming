	/*Max of array*/
#include <mpi.h>
#include <stdio.h>      /* printf, NULL */
#include <stdlib.h>     /* srand, rand */

 int main (int argc, char* argv[])
{
  int rank, size;
  const int N = 11;
  int vector[N];
  MPI_Status status;

  for (int i = 0 ; i < N; i++) {
  	vector[i] = rand() % 110;
  	printf("%d, ", vector[i]);
  }

  MPI_Init (&argc, &argv);       
  MPI_Comm_rank (MPI_COMM_WORLD, &rank);        
  MPI_Comm_size (MPI_COMM_WORLD, &size); 

  int localMax = vector[0];
  for(int i = rank; i < N; i += size)
  {
  	if (localMax < vector[i])
 		localMax = vector[i];
  }

  if (rank == 0) {
  	for (int i = 1; i < size; i++)
  	{
  		int recievedMax;
  		MPI_Recv(&recievedMax, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
  		if (recievedMax > localMax)
  			localMax = recievedMax;
  	}
  }
  else {
  	MPI_Send(&localMax, 1, MPI_INT, 0, 1, MPI_COMM_WORLD);
  }
  MPI_Finalize();
  if (rank == 0) {
  	printf("\nTotal max is: %d\n", localMax);
  }
  return 0;
}
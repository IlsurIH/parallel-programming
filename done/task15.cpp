#include <mpi.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>

int main(int argc, char *argv[]) {

	int n = 11;

	srand(time(0));
	int *a = new int[100];
	for (int i = 0; i < n; i++) {
		a[i] = rand()%100;
	}

  int rank, size;

  MPI_Status status;
  MPI_Init(&argc,&argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  if (rank != size-1) {
    for (int i = rank; i < n/2; i+=(size-1)) {
      swap(a[i], a[n-1-i]);
    }
    MPI_Send(a, n, MPI_INT, size-1, 0, MPI_COMM_WORLD);
  } else {
    printf("Array: ");
    for (int i = 0; i < n; i++) {
      printf("%d ", a[i]);
    }
    printf("\n");
    for (int i = 0; i < size-1; i++) {
      int *tempArr = new int[n];
      MPI_Recv(tempArr, n, MPI_INT, i, 0, MPI_COMM_WORLD, &status);
      for (int j = i; j < n/2; j+=(size-1)) {
        a[j] = tempArr[j];
        a[n-1-j] = tempArr[n-1-j];
      }
    }

    printf("Result:\n");
    for (int i = 0; i < n; i++) {
      printf("%d ", a[i]);
    }
    printf("\n");
  }
  MPI_Finalize();
  return 0;
}
#include <mpi.h>
#include <stdio.h>      /* printf, NULL */
#include <stdlib.h>     /* srand, rand */

 int main (int argc, char* argv[])
{
  int rank, size;
  MPI_Status status;
  MPI_Init (&argc, &argv);       
  MPI_Comm_rank (MPI_COMM_WORLD, &rank);        
  MPI_Comm_size (MPI_COMM_WORLD, &size); 

  int token;
  if (rank == 0) {
    int start = 0;
    MPI_Send(&start, 1, MPI_INT, (rank + 1) % size, 0, MPI_COMM_WORLD);
  }
  for (int i = 0; i < 20; i ++) {
    MPI_Recv(&token, 1, MPI_INT, MPI_ANY_SOURCE, 0,
             MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    token++;
    printf("Process %d received token %d from process %d\n",
           rank, token, (rank - 1) % size);
    MPI_Send(&token, 1, MPI_INT, (rank + 1) % size, 0, MPI_COMM_WORLD);
  }
}
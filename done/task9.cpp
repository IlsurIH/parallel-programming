#include <mpi.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>


int main(int argc, char **argv) {
	int size, rank, src, size, count, sim(1), cur;
	int n(4), i, j;
	int **a = new int *[n];
	int *buf = new int[n];
	for (i = 0; i < n; i++) {
		a[i] = new int[n];
	}
	MPI_Status status;
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	src = 0;
	if (rank == 0) {
		if (size == 1) {
			size = n / size;
		} else {
			size = n / (size - 1) + 1;
		}
		count = size; 
		for (i = 0; i < n; i++) {
			for (j = 0; j < n; j++) {
				a[i][j] = i;
			}
		}
		printf("A: \n");
		for (i = 0; i < n; i++) {
			for (j = 0; j < n; j++) {
				printf("%d ", a[i][j]);
			}
			printf("\n");
		}
		for (i = 1; i < size; i++) {
			if (i * size > n) {
				count = n - (i - 1) * size;
				if (count < 0) {
					count = 0;
				}
			}
			MPI_Send(&count, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
			for (j = 0; j < count; j++) {
				MPI_Send(&a[(i - 1) * (size) + j][0], n, MPI_INT, i, j + 1, MPI_COMM_WORLD);
				for(int k = 0; k < n; k++){
					buf[k] = a[k][(i - 1) * (size) + j];
				}
				MPI_Send(&buf[0], n, MPI_INT, i, 2*j + 1, MPI_COMM_WORLD);
			}
			MPI_Recv(&cur, 1, MPI_INT, i, n + 1, MPI_COMM_WORLD, &status);
			if(cur != 1){
				sim = 0;
			}
		}
		if(sim == 1){
			printf("Matrix is simmetric \n");
		} else {
			printf("Matrix is not simmetric \n");
		}
	} else {
		int num;
		MPI_Recv(&num, 1, MPI_INT, src, 0, MPI_COMM_WORLD, &status);
		int **inmsg1 = new int *[num];
		int **inmsg2 = new int *[num];
		for (j = 0; j < num; j++) {
			inmsg1[j] = new int[n];
			inmsg2[j] = new int[n];
			MPI_Recv(&inmsg1[j][0], n, MPI_INT, src, j + 1, MPI_COMM_WORLD, &status);
			MPI_Recv(&inmsg2[j][0], n, MPI_INT, src, 2*j + 1, MPI_COMM_WORLD, &status);
		}
		int flag = 1;//true
		for (i = 0; i < num; i++) {
			for (j = 0; j < n; j++) {
				if(inmsg1[i][j] != inmsg2[i][j]){
					flag = 0;//false
				}
			}
		}
		MPI_Send(&flag, 1, MPI_INT, src, n + 1, MPI_COMM_WORLD);
		}
	}
	MPI_Finalize();
	return 0;
}
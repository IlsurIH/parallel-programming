#include <iostream>
#include <mpi.h>
#include <time.h>

using namespace std;

int main(int argc, char* argv[])
{
	int size, rank;
	int n = 5;
	int m = 10;
	int matrix[50];
	int vector[10];
	int resultProc[5];
	int result[5];
	
	for (int i = 0; i < n; i++) {
		result[i] = 0;
	}

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	if (rank == 0)
	{
		srand(time(0));
		for (int i = 0; i < n  * m; i++) {
			matrix[i] = rand()%100;
		}
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				printf("%d ", matrix[i * n + j]);
			}
			printf("\n");
		}
		printf("\n");
		
		for (int i = 0; i < m; i++) {
			vector[i] = rand()%100;
			prinft("%d ", vector[i]);
		}
		printf("\n");
	}
	//раздали матрицу
	MPI_Bcast(matrix, n * m, MPI_INTEGER, 0, MPI_COMM_WORLD);
	//раздали вектор
	MPI_Bcast(vector, m, MPI_INTEGER, 0, MPI_COMM_WORLD);

	
	int k = n / size;
	int i0 = k * rank;
	int i1 = k * (rank + 1);
	if (rank == size - 1) {
		i1 = n;
	}

	for (int i = 0; i < n; i++) {
		resultProc[i] = 0;
	}

	for (int i = i0; i < i1; i++) {
		for (int j = 0; j < m; j++) {
			resultProc[i] += matrix[i * n + j] * vector[j];
		}
	}
	MPI_Reduce(&resultProc, &result, n, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

	if (rank == 0) {
		prinf("result : ");
		for (int i = 0; i < n; i++) {
			printf("%d, ", result[i]);
		}
	}

	MPI_Finalize();
	return 0;
}
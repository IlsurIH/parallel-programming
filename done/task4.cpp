 /* Scalar multiply*/
#include <mpi.h>
#include <stdio.h>      /* printf, NULL */
#include <stdlib.h>     /* srand, rand */

 int main (int argc, char* argv[])
{
  int rank, size;
  MPI_Status status;
  const int N = 10;
  int a[N], b[N];

  for (int i = 0 ; i < N; i++) {
  	a[i] = i;
  	printf("%d, ", a[i]);
  }
  printf("\n");

  for (int i = 0 ; i < N; i++) {
  	b[i] = i;
  	printf("%d, ", b[i]);
  }
  printf("\n");

  MPI_Init (&argc, &argv);       
  MPI_Comm_rank (MPI_COMM_WORLD, &rank);        
  MPI_Comm_size (MPI_COMM_WORLD, &size); 

  int localSum = 0;
  for (int i = rank; i < N; i += size)
  {
  	localSum += a[i] * b[i];
  }

  	printf("localSum: %d, ", localSum);
  if(rank == 0)
  {
  	for (int i = 1; i < size; i++)
  	{
  		int recievedSum;
  		MPI_Recv(&recievedSum, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
  		localSum += recievedSum;
  	}
  	printf("sum is: %d\n", localSum);
  }
  else 
  {
  	MPI_Send(&localSum, 1, MPI_INT, 0, 1, MPI_COMM_WORLD);
  }
}